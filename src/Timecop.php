<?php
declare(strict_types=1);

namespace Timecop;


final class Timecop
{
    private static ?self $instance = null;
    private const WELCOME = 'Start';
    private array $timeLog;

    private float $zeroTime;
    private float $lastTime;

    private int $trackerNo;


    public static function start(string $welcome = self::WELCOME): self
    {
        if (self::$instance === null) {
            self::$instance = new self($welcome);
        }
        return self::$instance;
    }

    public function __construct(string $desc = 'Start')
    {
        $this->zeroTime = $this->fixMicrotime();
        $this->lastTime = $this->zeroTime;
        $this->trackerNo = 0;
        $this->add($desc);
    }

    public function add(string $desc = ''): void
    {
        $time = $this->fixMicrotime();
        $tracker_no = $this->trackerNo;
        $from_start = $time - $this->zeroTime;
        $from_last = $time - $this->lastTime;

        $this->timeLog[] = compact('tracker_no', 'from_start', 'from_last', 'desc');

        $this->lastTime = $time;
        $this->trackerNo++;
    }

    public function getTimeLog(): array
    {
        return $this->timeLog;
    }

    private function fixMicrotime(): float
    {
        return round(microtime(true) * 1000, 4);
    }
}